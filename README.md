# FetchThat - A simple, lightweight API client. #
---
Requires PHP 5.6 >=

FetchThat is a simple little API client initially made for bing, but can be used for many other calls. For now, it only works with JSON responses.

```
$fetch = new FetchThat();
$fetch->get( $endPoint, $options );
```
The $options parameter should be an array containing `header` and `parameters`. This will build the URL, as well as set the content stream for HTTP.


## Example - Bing.com Custom Site Search ##
------


Below we can set our options, including the header & parameters. 

```
$endPoint = 'https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/search';
$options = [
        'header'        =>  [ 'Ocp-Apim-Subscription-Key'   =>  MYAPIKEY ],
        'parameters'    =>  [
            'q'             =>   rawurlencode( 'my search term' ),
            'customConfig'  =>  MYCONFIGKEY,
            'count'         =>  50,
            
        ]
    ];

$fetchThat = new FetchThat();
$results = $fetchThat->get( $endPoint, $options );
```


## Response Types ##
------

The default `get()` method will always return a `FetchApiResponse` containing the raw response from the call.

** to Array: **

Change the response to an array:

`$results->toArray();`


## FetchThat Methods ##
------

Public - **get**

`get( String $endPoint, Array $options = null );`

**Params**

* (String) (required) **$endPoint** - This is the target API endpoint to send the request.
* (Array) (optional) **$options** - this is the array of values to build out sections of the request, like parameters & header. 

The `$options` array accepts the following indexes:

 * `header` - (array) this is an array of `$key => $value` pairs that will create the headers of the request.
 * `parameters` - (array) this is an array of `$key => $value` pairs that will be url encoded into a request string
 
 **Returns**
 A `FetchApiResponse` object containing the raw response. 
 
 ------

Public - **showRequestUrl**

`showRequestUrl();`

 **Returns**
(string) String of the complete URL including endpoint and parameters.
 
------
 
 Public - **setParameters**

`setParameters( Array $parameters );`

This is an optional way to set only the parameters of the request. 

**Params**

* (required) **$parameters** - (array) this is an array of `$key => $value` pairs that will be set to the object's `parameters` property.
 
 **Returns**
 (object) `FetchThat` object. 
 
 ------

 Public - **setHeader**

`setHeader( Array $header );`

This is an optional way to set only the header of the request. 

**Params**

* (required) **$header** - (array) this is an array of `$key => $value` pairs that will be set to the object's `header` property.
 
 **Returns**
 (object) `FetchThat` object. 
 
 ------