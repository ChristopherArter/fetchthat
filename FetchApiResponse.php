<?php 

class FetchApiResponse {

    public $response;
    public $endPoint; 
    public $responseCode;
    // constructor
    function __construct( $response ){
        $this->response = $response;
        return $this->response;
    }

    /**
     * Take JSON response and decode to Array.
     *
     * @param String $options - pass options to json_decode funciton.
     * @return Array 
     */
    public function toArray( String $options = null ){
        return json_decode( $this->response, true, 512, $options );
    }

    public function getHttpStatus(){
       return $this->responseCode;
    }

    /**
     * Maps the response body to loop items. 
     *
     * @param String $mapModel
     * @return Array Returns array of StdClass objects. 
     */
    public function mapTo( String $mapModel ){

        $model = new FetchResponseModel( $this->endPoint, $this->toArray() );
        $this->results = $model->results;
        return $model->results;
    }

}