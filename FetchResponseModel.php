<?php 

class FetchResponseModel {

    public $results;
    public $endPoint;
    public $model; 
    public $knownEndpoints;

    function __construct( String $endPoint, Array $results ){

        $this->knownEndPoints = [

            'bing_custom_search'    =>  [
                'end_point'     =>  'https://api.cognitive.microsoft.com/bingcustomsearch/v7.0/search',
                'model'         =>  'bing', // name of the method to match this endpoint to.
                'api_version'   =>  'v7'
            ],
    
            'google_custom_search'  =>  [
                'end_point' =>  'https://www.googleapis.com/customsearch/v1?parameters',
                'model'     =>  'google',
            ]
        ];

        $this->endPoint = $endPoint;
        $this->results = $this->resolveResultsByEndPoint( $results );
        $this->model = $this->getModelFromEndpoint( $this->endPoint );
        $this->results = $this->setResults( $this->model, $this->results );
        return $this->results;
    }

    /**
     * get the method associated with the end point
     *
     * @param String $endPoint
     * @return String | Null 
     */
    public function getModelFromEndpoint() {
        foreach ( $this->knownEndPoints as $providor ){
            if ( array_search( $this->endPoint, $providor ) ){
                return $providor['model'];
            }
        }
    }

    protected function resolveResultsByEndPoint( Array $results ){
        $model = $this->getModelFromEndpoint( $this->endPoint );

        if ( $model == 'bing' ){
            $this->results = $results['webPages']['value'];
        }

        return $this->results;

    }


    /**
     * returns the output from a method for the model associated with that endpoint
     *
     * @param String $endPoint
     * @param Array $input
     * @return void
     */
    public function getModel( String $endPoint, Array $input ){
        
        $modelFromEndpoint = $this->getlModelFromEndpoint( $endPoint );
        return $this->$model( $input );
    }

    public function setResults( String $model, Array $inputs ){
        $results = [];
        
        foreach ( $inputs as $input ) {

            $modelInput = $this->$model( $input );
            
            array_push( $results, $modelInput );
        }
        return $results;
    }

    /**
     * Checks if the input exists and returns, else sets to null. This is a hack to prevent errors in *inconsistent input.
     *
     * @param String $key
     * @param [type] $input
     * @param [type] $fallback
     * @return void
     */
    protected function existsOrNull( String $key, $input, $fallback = null ){
        if( isset( $input ) ){
            return $input;
        }
        return $fallback;
    }

    /**
     * Bing custom search response model as of api v7. 
     *
     * @param Array $input
     * @return Array $response
     */
    public function bing( Array $input ){
        
        $response = [
            'title'         =>  $input['name'],
            'thumbnail'     =>  'thumbnailhere',
            'snippet'       =>  $input['snippet' ],
            'url'           =>  $input['url'],
            'related_links' =>  'deeplinks'
        ];
        
        $response = (object) $response;
        return $response;

    }

}