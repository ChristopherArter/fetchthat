<?php

class FetchThat {

    public $header; 
    public $parameters;
    public $endPoint;
    public $requestUrl;

    function __construct(){
        require_once('FetchApiResponse.php');
        require_once('FetchResponseModel.php');
    }

    /**
     * assembles the query and returns data
     *
     * @param string $query
     * @param array $objectParameters
     * @return mixed response
     */
    public function get( String $endPoint, Array $objectParameters = null ) {
       
        $this->prepareRequest( $endPoint, $objectParameters );
        
        $curlOptions = [
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_URL             => $this->requestUrl
        ];

        // if we've got a header passed to $objectParameters or the header has been set with setHeader method
        if( is_array( $this->header ) || isset( $this->header ) ) {
            $curlOptions[CURLOPT_HTTPHEADER] = $this->header;
        }
        $request = $this->curl( $curlOptions );

        $response = new FetchApiResponse( $request );
        $response->responseCode = $this->responseCode;
        $response->endPoint = $this->endPoint;
        return $response;
    }

    /**
     * Sets the parameters header, and endPoint.
     * @param  String     $endPoint         Target endpoint
     * @param  Array|null $objectParameters Params passed to create request
     */
    private function prepareRequest( String $endPoint, Array $objectParameters = null ){

        // set parameters
        if( isset( $objectParameters['parameters'] ) ){

            $this->setParameters( $objectParameters['parameters'] );

        } else if ( is_array( $objectParameters ) ){

            $this->setParameters( $objectParameters );
        }

        // set endpoint
        $this->setEndPoint( $endPoint );

        // set the url with parameters passed.        
        $this->requestUrl = $this->formatUrl( $this->endPoint, $this->parameters);
 
        // set header
        if( isset( $objectParameters['header'] ) ){

            $this->setHeader( $objectParameters['header'] );

        }

    }
    
    /**
     * Create curl
     *
     * @param Array $curlOptions
     * @return response
     */
    protected function curl( Array $curlOptions = null ){
        
        $curl = curl_init();
        curl_setopt_array( $curl, $curlOptions );
        $response = curl_exec( $curl );
        $this->responseCode = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
        return $response;
        
    }

    /**
     * formats the url to include the parameters
     *
     * @param String $url
     * @param String $query
     * @param Array $parameters
     * @return string $formattedUrl
     */
    protected function formatUrl( String $url, Array $parameters = null ){
        
        $urlParameters = '';

        if( $parameters ){
            $count = 1;
            foreach( $parameters as $key => $value ){

                $separatorCharacter = '?';
                
                if( $count > 1 ){
                    $separatorCharacter = '&';
                }

                $paramString = $separatorCharacter . $key . '=' . $value . '';
                $urlParameters .= $paramString;
                
                $count++;
            }
        }
        $formattedUrl = $url . $urlParameters;
        return $formattedUrl;
    }

    /**
     * set the parameters for the url
     *
     * @param Array $parameters
     * @return Object FetchAPI
     */
    public function setParameters( Array $parameters ){

        $params = [];
            foreach ($parameters as $key => $value){
                $params[$key] = $value;
            }
            $this->parameters = $params;
            return $this;
    }
    /**
     * set the parameters for the header
     *
     * @param Array $header
     * @return Object FetchAPI
     */
    public function setHeader( Array $header ){
        $requestHeader = [];
        foreach($header as $key => $value){
            $string = $key .':'. $value;
            array_push($requestHeader, $string);
        }
        $this->header = $requestHeader;
        return $this;
    }

    /**
     * show the request url 
     *
     * @return string
     */
    public function showRequestUrl(){
        return $this->requestUrl;
    }

    /**
     * set the url to send the request to.
     *
     * @param String $url
     * @return void
     */
    public function setEndPoint( String $url ){
        $this->endPoint = $url;
        return;
    }

}